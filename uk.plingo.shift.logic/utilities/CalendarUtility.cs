﻿using System;
using uk.plingo.shift.core.entities;

namespace uk.plingo.shift.logic.utilities
{
    public class CalendarUtility : BaseUtility<Calendar>
    {
        public static Calendar CreateCalendar(string eventName, string eventNotes, DateTime eventDateTime, int eventDuration, int userCreatedId)
        {
            return new Calendar()
            {
                EventDateTime = eventDateTime,
                EventDuration = eventDuration,
                EventName = eventName,
                EventNotes = eventNotes,

                DateCreated = DateTime.Now,
                UserCreated = userCreatedId,
                Status = core.interfaces.entities.Entity.EntityState.Active
            };
        }
    }
}