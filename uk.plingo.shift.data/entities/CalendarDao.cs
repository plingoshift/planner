﻿using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces.data;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.data.entities;

namespace uk.plingo.shift.data
{
    public class CalendarDao : BaseDao<Calendar>, ICalendarDao
    {
        public CalendarDao(ISession session) : base(session)
        {
        }
    }
}