﻿using uk.plingo.shift.core.interfaces;
using uk.plingo.shift.core.interfaces.data;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.data;

namespace uk.plingo.shift.entity.data
{
    public class EntityDatabase : IDatabase
    {
        private ISession _session;

        public EntityDatabase()
        {
        }

        public ICalendarDao CalendarDao => new CalendarDao(_session);

        public IEventTypeDao EventTypeDao => new EventTypeDao(_session);

        public void ResetSession()
        {
            _session = null;
        }

        public void SetSession(ISession session)
        {
            _session = session;
        }
    }
}