using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.data;
using uk.plingo.shift.data.test.helpers;

namespace uk.plingo.shift.test.data
{
    [TestClass]
    public class CalendarDaoTests
    {
        [TestMethod]
        public void StatusItems()
        {
            IList<Calendar> calendars = new List<Calendar>();
            calendars.Add(new Calendar()
            {
                id = 1,
                Status = core.interfaces.entities.Entity.EntityState.Active
            });
            calendars.Add(new Calendar()
            {
                id = 2,
                Status = core.interfaces.entities.Entity.EntityState.Deleted
            });

            MockSession<Calendar> mockSession = new MockSession<Calendar>(calendars);
            CalendarDao calendarDao = new CalendarDao(mockSession);

            var allCalendarItems = calendarDao.GetAll();
            Assert.AreEqual(2, allCalendarItems.Count());

            var activeCalendarItems = calendarDao.GetByStatus(core.interfaces.entities.Entity.EntityState.Active);
            Assert.AreEqual(1, activeCalendarItems.Count());

            var deletedCalendarItems = calendarDao.GetByStatus(core.interfaces.entities.Entity.EntityState.Deleted);
            Assert.AreEqual(1, deletedCalendarItems.Count());
        }
    }
}