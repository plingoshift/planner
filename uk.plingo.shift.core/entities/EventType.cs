﻿using System;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.core.entities
{
    public class EventType : Entity
    {
        public string EventTypeName { get; set; }
        public string EventNotes { get; set; }
        public TimeSpan StartTime { get; set; }
        public int? EventDuration { get; set; }
    }
}