using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.data.entities;
using uk.plingo.shift.data.test.helpers;

namespace uk.plingo.shift.test.data
{
    [TestClass]
    public class BaseDaoTests
    {
        [TestMethod]
        public void GetById()
        {
            IList<Calendar> calendars = CalendarDatabaseHelper.DefaultItems();

            MockSession<Calendar> mockSession = new MockSession<Calendar>(calendars);
            MockDao mockDao = new MockDao(mockSession);

            var itemOne = mockDao.GetById(1);
            Assert.AreEqual(1, itemOne.id);

            var itemTwo = mockDao.GetById(2);
            Assert.AreEqual(2, itemTwo.id);

            var itemThree = mockDao.GetById(3);
            Assert.IsNull(itemThree);
        }

        [TestMethod]
        public void StatusItems()
        {
            IList<Calendar> calendars = CalendarDatabaseHelper.DefaultItems();

            MockSession<Calendar> mockSession = new MockSession<Calendar>(calendars);
            MockDao mockDao = new MockDao(mockSession);

            var allCalendarItems = mockDao.GetAll();
            Assert.AreEqual(2, allCalendarItems.Count());

            var activeCalendarItems = mockDao.GetByStatus(core.interfaces.entities.Entity.EntityState.Active);
            Assert.AreEqual(1, activeCalendarItems.Count());

            var deletedCalendarItems = mockDao.GetByStatus(core.interfaces.entities.Entity.EntityState.Deleted);
            Assert.AreEqual(1, deletedCalendarItems.Count());
        }

        [TestMethod]
        public void AddItem()
        {
            IList<Calendar> calendars = CalendarDatabaseHelper.DefaultItems();

            MockSession<Calendar> mockSession = new MockSession<Calendar>(calendars);
            MockDao mockDao = new MockDao(mockSession);

            var allCalendarItems = mockDao.GetAll();
            Assert.AreEqual(2, allCalendarItems.Count());

            var newCalendarItem = mockDao.Add(new Calendar()
            {
                Status = core.interfaces.entities.Entity.EntityState.Active
            });

            Assert.IsNotNull(newCalendarItem);

            var newItemFromDb = mockDao.GetById(newCalendarItem.id);
            Assert.IsNotNull(newItemFromDb);

            Assert.AreEqual(3, newItemFromDb.id);
        }

        public class MockDao : BaseDao<Calendar>
        {
            public MockDao(ISession session) : base(session)
            {
            }
        }
    }
}