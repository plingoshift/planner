﻿using System.Collections.Generic;
using System.Linq;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.data.test.helpers
{
    public class MockSession<S> : ISession where S : Entity
    {
        public IList<S> Items { get; private set; }

        public MockSession(IList<S> items)
        {
            Items = items;
        }

        public T AddOrUpdate<T>(T item) where T : Entity
        {
            if (item is S)
            {
                int id = 0;

                if (Items.Any())
                    id = Items.Max(x => x.id);

                id += 1;

                item.id = id;
                Items.Add(item as S);
            }

            return item;
        }

        public IEnumerable<T> CreateQuery<T>() where T : Entity
        {
            if (Items is IList<T>)
                return Items as IEnumerable<T>;
            else
                return null;
        }
    }
}