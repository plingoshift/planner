﻿using uk.plingo.shift.core.interfaces;

namespace uk.plingo.shift.logic
{
    public class BaseLogic
    {
        protected IDatabase Database { get; private set; }

        public BaseLogic(IDatabase database)
        {
            Database = database;
        }
    }
}