﻿using System;
using uk.plingo.shift.core.entities;

namespace uk.plingo.shift.logic.utilities
{
    public class EventTypeUtility : BaseUtility<EventType>
    {
        public static EventType CreateEventType(string eventTypeName, string eventNotes, TimeSpan eventStartTime, int eventDuration, int userCreatedId)
        {
            return new EventType()
            {
                EventTypeName = eventTypeName,
                StartTime = eventStartTime,
                EventDuration = eventDuration,
                EventNotes = eventNotes,

                DateCreated = DateTime.Now,
                UserCreated = userCreatedId,
                Status = core.interfaces.entities.Entity.EntityState.Active
            };
        }
    }
}