﻿using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces;
using uk.plingo.shift.core.interfaces.data;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.data.test.helpers;

namespace uk.plingo.shift.data.test
{
    public class MockDatabase : IDatabase
    {
        public MockDatabase()
        {
            CalendarDao = new CalendarDao(new MockSession<Calendar>(CalendarDatabaseHelper.DefaultItems()));
            EventTypeDao = new EventTypeDao(new MockSession<EventType>(EventTypeDatabaseHelper.DefaultItems()));
        }

        public ICalendarDao CalendarDao { get; set; }

        public IEventTypeDao EventTypeDao { get; set; }

        public void ResetSession()
        {
            // do nothing
        }

        public void SetSession(ISession session)
        {
            // do nothing
        }
    }
}