﻿using uk.plingo.shift.core.interfaces.data;
using uk.plingo.shift.core.interfaces.database;

namespace uk.plingo.shift.core.interfaces
{
    public interface IDatabase
    {
        public ICalendarDao CalendarDao { get; }
        public IEventTypeDao EventTypeDao { get; }

        public void SetSession(ISession session);
        public void ResetSession();
    }
}