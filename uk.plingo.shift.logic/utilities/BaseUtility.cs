﻿using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.logic.utilities
{
    public class BaseUtility<T> where T : Entity
    {
        public static void UpdateStatus(T item, Entity.EntityState state)
        {
            item.Status = state;
        }

        public static void Delete(T item)
        {
            UpdateStatus(item, Entity.EntityState.Deleted);
        }

        public static void Active(T item)
        {
            UpdateStatus(item, Entity.EntityState.Active);
        }
    }
}