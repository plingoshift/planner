# Plingo Shift #

* This is plingo shift - a shift sharing calendar application that is currently under development.  
* Version 0

### How do I get set up? ###

* Clone the repository
* Open Visual Studio 
* Set the database connection string
* Hit Run


### More Information ###

* Currently I have been writing a blog on the progress. This should take you through design choices etc. If required.  this can be found at https://plingo.uk